<div id="wrapper">
  <center>
	 <nav>
		<ul>
		  <li><?php echo $this->Html->link("Info",array('controller' =>'Users','action' => 'info1'));?></li>
		  <li><?php echo $this->Html->link("Add Salary",array('controller' =>'Salaries','action' =>'admin_add_salary'));?></li>
      <li><a href="#">Salary Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_salaries'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_salaries','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_salaries','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_salaries','4'));?></li>	
          </ul>	
		  </li>
		  <li><a href="#">Increment Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' =>'admin_view_increments'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' =>'admin_view_increments','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' =>'admin_view_increments','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_increments','4'));?></li>  
          </ul>   
      </li>
		</ul>
	 </nav>
	</center>
</div>

<!-- Show Salary Increment details of requested User to Admin  -->
<div style="margin-top:100px !important">
<?php
if(empty($increments)) { ?>
  <center><legend>No Records Found</legend></center>
<?php }
else { ?>
<?php foreach($increments as $increment): ?>
  <fieldset class="field_set1">
  <h1>Increment Date: <?php echo $increment['Increment']['incr_date']; ?></h1>
  <table>
    <tr>
      <td>Old Basic Salary</td>
      <td>Increment Given</td>
      <td>New Basic Salary</td>
    </tr>
    <tr>
      <td><?php echo $increment['Increment']['old_sal']; ?></td>
      <td><?php echo $increment['Increment']['increment']; ?></td>
      <td><?php echo $increment['Increment']['new_sal']; ?></td>
    </tr>

  </table>
    </fieldset>
<?php endforeach; 
 } ?>
</div>
<!-- Show ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>