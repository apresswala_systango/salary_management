<div id="wrapper">
    <center>
       <nav>
          <ul>
             <li><?php echo $this->Html->link("Home",array('controller' => 'Users','action' => 'index'));?></li>
             <li><?php echo $this->Html->link("Details",array('controller' => 'Users','action' => 'user_details'));?></li>
             <li><?php echo $this->Html->link("Salary",array('controller' => 'Calculates','action' => 'user_salary'));?></li>
             <li><a href="#">Increments</a></li>
             <li><?php echo $this->Html->link("Change Details",array('controller' => 'Users','action' => 'user_change_details'));?> </li>
             <li><?php echo $this->Html->link("Change Password",array('controller' => 'Users','action' => 'change_password'));?></li>
          </ul>
       </nav>
     </center>
</div>

<!-- Show Salary Increment details to user -->
<div style="margin-top:100px !important">
<?php
if(empty($increments)) { ?>
	<center><legend>No Records Found</legend></center>
<?php }
else { ?>
<?php foreach($increments as $increment): ?>
	<fieldset class="field_set1">
	<h1>Increment Date: <?php echo $increment['Increment']['incr_date']; ?></h1>
	<table>
		<tr>
			<td>Old Basic Salary</td>
			<td>Increment Given</td>
			<td>New Basic Salary</td>
		</tr>
		<tr>
			<td><?php echo $increment['Increment']['old_sal']; ?></td>
			<td><?php echo $increment['Increment']['increment']; ?></td>
			<td><?php echo $increment['Increment']['new_sal']; ?></td>
		</tr>

	</table>
    </fieldset>
<?php endforeach; 
 } ?>
</div>
<!-- Show ends -->