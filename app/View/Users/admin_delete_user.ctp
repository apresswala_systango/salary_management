<div id="wrapper">
 <center>
	<nav>
		<ul>
		   <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'info'));?></li>
		   <li><a href="#">View Users</a>
          <ul>
             <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_user'));?></li>
             <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_user','2'));?></li>
             <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_user','3'));?></li>
             <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_user','4'));?></li>	
          </ul>	
		   </li>
		   <li><?php echo $this->Html->link("Add Users",array('controller' => 'Users','action' => 'admin_add_user'));?></li>
		   <li><a href="#">Delete Users</a>
           <ul>
             <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_delete_user'));?></li>
             <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_delete_user','2'));?></li>
             <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_delete_user','3'));?></li>
             <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_delete_user','4'));?></li>  
           </ul>   
       </li>
		   <li><a href="#">Disable Users</a>
           <ul>
             <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_disable_user'));?></li>
             <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_disable_user','2'));?></li>
             <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_disable_user','3'));?></li>
             <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_disable_user','4'));?></li>  
           </ul>   
       </li>
		</ul>
	</nav>
 </center>
</div>

<!-- Delete Users -->
<div style="margin-top:100px !important">
<?php
      if($users==null) { ?>
 	      <center><h1><?php echo "No Records Found";?></h1></center>
<?php }
      else {
?>
<?php echo $this->Form->create('User',array('action'=>'delete_selected')); ?>
  <?php foreach ($users as $user): ?>
    <fieldset class="field_set_main">
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <?php echo $this->Form->checkbox('Users.'.$user['User']['id'], array('value' => $user['User']['id'],'hiddenField' => false));  ?> 
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <?php echo "User Id : {$user['User']['id']}"; ?>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <?php echo "Username : {$user['User']['username']}"; ?>           
    </fieldset>
  <?php endforeach; ?>
<center><?php echo $this->Form->end(__('Delete')); ?></center>
<?php } ?>
</div>
<!-- Delete ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>




