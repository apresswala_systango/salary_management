<div class="users form">

<?php echo $this->Form->create('User', array('action' => 'edit/'.$user['User']['id'])); ?>
    <fieldset>
    <?php 
      echo $this->Form->input('id', array('type' => 'hidden','default'=>$user['User']['id']));
      echo $this->Form->input('username',array('default'=>$user['User']['username']));
      echo $this->Form->input('password',array('default'=>$user['User']['password']));
      echo $this->Form->input('firstname',array('default'=>$user['User']['firstname']));
      echo $this->Form->input('lastname',array('default'=>$user['User']['lastname']));
      echo $this->Form->input('email',array('default'=>$user['User']['email']));
      echo $this->Form->input('contact',array('default'=>$user['User']['contact']));
      echo $this->Form->input('department_id',array('default'=>$user['User']['department_id'],'options' => array('1','2','3','4')));
      echo $this->Form->input('role', array('default'=>$user['User']['role'],'options' => array('admin' => 'admin', 'user' => 'user')));
      echo $this->Form->input('doj',array('default'=>$user['User']['doj']));
      echo $this->Form->input('created',array('default'=>$user['User']['created']));
    ?>
    </fieldset>
<?php echo $this->Form->end('Edit User'); ?>

</table>    

</div>
