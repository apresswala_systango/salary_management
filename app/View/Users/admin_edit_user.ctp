<div id="wrapper">
 <center>
  <nav>
    <ul>
       <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'info'));?></li>
       <li><a href="#">View Users</a>
          <ul>
             <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_user'));?></li>
             <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_user','2'));?></li>
             <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_user','3'));?></li>
             <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_user','4'));?></li>  
          </ul> 
       </li>
       <li><?php echo $this->Html->link("Add Users",array('controller' => 'Users','action' => 'admin_add_user'));?></li>
       <li><a href="#">Delete Users</a>
           <ul>
             <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_delete_user'));?></li>
             <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_delete_user','2'));?></li>
             <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_delete_user','3'));?></li>
             <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_delete_user','4'));?></li>  
           </ul>   
       </li>
       <li><a href="#">Disable Users</a>
           <ul>
             <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_disable_user'));?></li>
             <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_disable_user','2'));?></li>
             <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_disable_user','3'));?></li>
             <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_disable_user','4'));?></li>  
           </ul>   
       </li>
    </ul>
  </nav>
 </center>
</div>

<?php
if(empty($user)) {
  $user=null;
}
?>
<!-- Form for updating user details by admin -->
<div style="margin-top:100px !important">
<fieldset class="field_set1">
  <legend>Update User</legend>
  <?php echo $this->Form->create('User');  
        echo $this->Form->input('id', array('type' => 'hidden','default'=>$user['User']['id']));
        echo $this->Form->input('username',array('default'=>$user['User']['username']));
        echo $this->Form->input('firstname',array('default'=>$user['User']['firstname']));
        echo $this->Form->input('lastname',array('default'=>$user['User']['lastname']));
        echo $this->Form->input('email',array('default'=>$user['User']['email']));
        echo $this->Form->input('contact',array('default'=>$user['User']['contact']));
        echo $this->Form->input('address',array('default'=>$user['User']['address']));
        echo $this->Form->input('department_id',array('default'=>$user['User']['department_id'],'options' => array(
          '1'=>'1','2'=>'2','3'=>'3','4'=>'4')));
        echo $this->Form->input('role', array('default'=>$user['User']['role'],'options' => array('admin' => 'admin','user' => 'user')));
        echo $this->Form->input('doj',array('default'=>$user['User']['doj']));
        echo $this->Form->input('created',array('default'=>$user['User']['created']));
        echo $this->Form->end('Edit User'); 
  ?>
</fieldset>
</div>
<!-- Form ends -->
   


