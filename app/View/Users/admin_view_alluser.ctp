<head>
    <title> 
    </title>
    <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css('ddemployee');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
<div id="wrapper">
     <center>
     <nav>
        <ul>
          <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'admin_index'));?></li>
          <li><a href="#">View Users</a>
            <ul>
              <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_alluser'));?></li>
              <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_dept','id'=>'2'));?></li>
              <li><?php echo $this->Html->link("Developement",array('controller' => 'Users','action' => 'user_details'));?></li>
              <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'user_details'));?></li>  
            </ul>   
          </li>
          <li><?php echo $this->Html->link("Add Users",array('controller' => 'Users','action' => 'admin_add_user'));?></li>
          <li><?php echo $this->Html->link("Delete Users",array('controller' => 'Increments','action' => 'user_increments'));?></li>
          <li><?php echo $this->Html->link("Disable Users",array('controller' => 'Users','action' => 'change_password'));?></li>
        </ul>
     </nav>
     </center>
</div>
</body>


<h1>Users</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>firstname</th>
        <th>lastname</th>
        <th>Action</th>
    </tr>


    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id']; ?></td> 
        <td>
         <?php
                echo $this->Html->link(
                    $user['User']['username'],
                    array('action' => 'view', $user['User']['id'])
                );
        ?>
        </td>
        <td><?php echo $user['User']['firstname']; ?></td>
        <td><?php echo $user['User']['lastname']; ?></td> 
         <td>
            <?php
                echo $this->Html->link(
                    "edit",
                    array('action' => 'admin_edit_user1', $user['User']['id'])
                );
            ?>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $user['User']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>


