<div id="wrapper">
    <center>
       <nav>
          <ul>
             <li><?php echo $this->Html->link("Home",array('controller' => 'Users','action' => 'index'));?></li>
             <li><?php echo $this->Html->link("Details",array('controller' => 'Users','action' => 'user_details'));?></li>
             <li><?php echo $this->Html->link("Salary",array('controller' => 'Calculates','action' => 'user_salary'));?></li>
             <li><?php echo $this->Html->link("Increments",array('controller' => 'Increments','action' => 'user_increments'));?> </li>
             <li><a href="#">Change Details</a></li>
             <li><?php echo $this->Html->link("Change Password",array('controller' => 'Users','action' => 'change_password'));?></li>
          </ul>
       </nav>
     </center>
</div>

<?php
if(empty($user)) {
  $user=null;
}
?>
<!-- Form for updating details -->
<div style="margin-top:100px !important">
<fieldset class="field_set1">
  <legend>Update Yourself</legend>
  <?php echo $this->Form->create('User')?>
      <?php 
      echo $this->Form->input('id', array('type' => 'hidden','default'=>$user['User']['id']));
      echo $this->Form->input('firstname',array('default'=>$user['User']['firstname']));
      echo $this->Form->input('lastname',array('default'=>$user['User']['lastname']));
      echo $this->Form->input('email',array('default'=>$user['User']['email']));
      echo $this->Form->input('contact',array('default'=>$user['User']['contact']));
      echo $this->Form->input('address',array('default'=>$user['User']['address']));
      ?>
   <?php echo $this->Form->end('Update'); ?>
</fieldset>
</div>
<!-- Form ends -->