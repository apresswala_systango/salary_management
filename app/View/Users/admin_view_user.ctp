<div id="wrapper">
  <center>
    <nav>
      <ul>
        <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'info'));?></li>
        <li><a href="#">View Users</a>
            <ul>
              <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_user'));?></li>
              <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_user','2'));?></li>
              <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_user','3'));?></li>
              <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_user','4'));?></li>  
            </ul>   
        </li>
        <li><?php echo $this->Html->link("Add Users",array('controller' => 'Users','action' => 'admin_add_user'));?></li>
        <li><a href="#">Delete Users</a>
            <ul>
              <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_delete_user'));?></li>
              <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_delete_user','2'));?></li>
              <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_delete_user','3'));?></li>
              <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_delete_user','4'));?></li>  
            </ul>   
        </li>
        <li><a href="#">Disable Users</a>
            <ul>
              <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_disable_user'));?></li>
              <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_disable_user','2'));?></li>
              <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_disable_user','3'));?></li>
              <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_disable_user','4'));?></li>  
            </ul>   
        </li>
      </ul>
    </nav>
  </center>
</div>


<!-- Display Users -->
<?php
    if($users==null) { ?>
     <center><h1><?php echo "No Records Found";?></h1></center>
<?php }
else { ?>

<div style="margin-top:100px !important">
<fieldset class="field_set1">
<legend>Users</legend>
<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>firstname</th>
        <th>lastname</th>
        <th>Action</th>
    </tr>

    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id']; ?></td> 
        <td>
         <?php
                echo $this->Html->link(
                    $user['User']['username'],
                    array('action' => 'admin_view_userdetails', $user['User']['id'])
                );
        ?>
        </td>
        <td><?php echo $user['User']['firstname']; ?></td>
        <td><?php echo $user['User']['lastname']; ?></td> 
        <td>
            <?php
                echo $this->Html->link(
                    "edit",
                    array('action' => 'admin_edit_user', $user['User']['id'])
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>
</fieldset>
</div>
<?php } ?>
<!-- Display ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>


