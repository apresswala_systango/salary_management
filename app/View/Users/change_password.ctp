<div id="wrapper">
    <center>
       <nav>
          <ul>
             <li><?php echo $this->Html->link("Home",array('controller' => 'Users','action' => 'index'));?></li>
             <li><?php echo $this->Html->link("Details",array('controller' => 'Users','action' => 'user_details'));?></li>
             <li><?php echo $this->Html->link("Salary",array('controller' => 'Calculates','action' => 'user_salary'));?></li>
             <li><?php echo $this->Html->link("Increments",array('controller' => 'Increments','action' => 'user_increments'));?> </li>
             <li><?php echo $this->Html->link("Change Details",array('controller' => 'Users','action' => 'user_change_details'));?> </li>
             <li><a href="#">Change Password</a></li>
          </ul>
       </nav>
     </center>
</div>

<?php
if(empty($user)) {
  $user['User']['id']=null;
} 
?> 
<!-- Form for Change Password --> 
<div style="margin-top:100px !important">
<fieldset class="field_set1">
  <legend>
       <?php echo ('Please Enter The New Password'); ?>
  </legend>
  <?php 
       echo $this->Form->create('User');  
       echo $this->Form->input('id', array('type' => 'hidden','default'=>$user['User']['id']));
       echo $this->Form->input('password',array('label' => 'New Password'));
       echo $this->Form->input('password_confirm',array('type' => 'password', 'label' => 'Confirm Password'));
	     echo $this->Form->end('Change');
	?> 
</fielset>
</div>
<!-- Form ends -->
<?php $this->validationErrors; ?> 