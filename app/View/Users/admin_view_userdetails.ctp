<div id="wrapper">
 <center>
	<nav>
		<ul>
		  <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'info'));?></li>
		  <li><a href="#">View Users</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_user'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_user','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_user','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_user','4'));?></li>	
          </ul>	
		  </li>
		  <li><?php echo $this->Html->link("Add Users",array('controller' => 'Users','action' => 'admin_add_user'));?></li>
		  <li><a href="#">Delete Users</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_delete_user'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_delete_user','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_delete_user','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_delete_user','4'));?></li>  
          </ul>   
      </li>
		  <li><a href="#">Disable Users</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_disable_user'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_disable_user','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_disable_user','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_disable_user','4'));?></li>  
          </ul>   
      </li>
		</ul>
	</nav>
 </center>
</div>

<!-- Details of Selected User -->
<div style="margin-top:100px !important">
<fieldset class="field_set1">
<legend>DETAILS</legend>	
<table>
    <tr>
        <td><?php echo "User id :" ?></td>
        <td><?php echo $user['User']['id']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Username :" ?></td>
        <td><?php echo $user['User']['username']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Password :" ?></td>
        <td><?php echo $user['User']['password']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Firstname :" ?></td>
        <td><?php echo $user['User']['firstname']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Lastname :" ?></td>
        <td><?php echo $user['User']['lastname']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Email id :" ?></td>
        <td><?php echo $user['User']['email']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Contact :" ?></td>
        <td><?php echo $user['User']['contact']; ?></td> 
    </tr> 
    <tr>
        <td><?php echo "Address :" ?></td>
        <td><?php echo $user['User']['address']; ?></td> 
    </tr> 
    <tr>
        <td><?php echo "Department :" ?></td>
        <td><?php echo $user['Department']['name']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Date of Joining :" ?></td>
        <td><?php echo $user['User']['doj']; ?></td> 
    </tr>
</table>
</fieldset>
</div>
<!-- Details end -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>
