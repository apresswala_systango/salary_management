<div id="wrapper">
    <center>
       <nav>
          <ul>
             <li><a href="#">Home</a></li>
             <li><?php echo $this->Html->link("Details",array('controller' => 'Users','action' => 'user_details'));?></li>
             <li><?php echo $this->Html->link("Salary",array('controller' => 'Calculates','action' => 'user_salary'));?></li>
             <li><?php echo $this->Html->link("Increments",array('controller' => 'Increments','action' => 'user_increments'));?> </li>
             <li><?php echo $this->Html->link("Change Details",array('controller' => 'Users','action' => 'user_change_details'));?> </li>
             <li><?php echo $this->Html->link("Change Password",array('controller' => 'Users','action' => 'change_password'));?></li>
          </ul>
       </nav>
     </center>
</div>

<!-- Welcome Msg to user -->
<div style="margin-top:100px !important">
<fieldset class="field_set1">
     <legend>Welcome</legend>
     <br>
     <label><?php echo "{$user['User']['firstname']} {$user['User']['lastname']}";?></label>
     <br><br>
     <label><?php echo "You joined the Organization on {$user['User']['doj']}";?><label>
     <br><br>
     <label><?php echo "Its been {$period} since you are working here.";?></label> 
</fieldset>
</div>
<!-- Msg ends -->

<!-- link only for Admin -->
<?php
    $role = $user['User']['role'];
    if($role=="admin") {
?>
    <div style="float:right; margin-right:10px; margin-bottom:10px">    
    <?php echo $this->Html->link("Admin Options",array('action' => 'admin_index')); ?>
    </div>
<?php } ?>
<!-- link end -->

  





