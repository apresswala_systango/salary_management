<div>
<?php echo $this->Form->create('User'); ?>
    <fieldset>
    <?php 
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('firstname');
        echo $this->Form->input('lastname');
        echo $this->Form->input('email');
        echo $this->Form->input('contact');
        echo $this->Form->input('department_id', array('options' => array('1'=>'1','2'=>'2','3'=>'3')));
        echo $this->Form->input('role', array('options' => array('admin' => 'admin', 'user' => 'user')));
        echo $this->Form->input('doj');
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
