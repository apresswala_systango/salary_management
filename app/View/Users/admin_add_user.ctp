<div id="wrapper">
  <center>
    <nav>
      <ul>
        <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'info'));?></li>
        <li><a href="#">View Users</a>
            <ul>
              <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_user'));?></li>
              <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_user','2'));?></li>
              <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_user','3'));?></li>
              <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_user','4'));?></li>  
            </ul>   
        </li>
        <li><?php echo $this->Html->link("Add Users",array('controller' => 'Users','action' => 'admin_add_user'));?></li>
        <li><a href="#">Delete Users</a>
            <ul>
              <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_delete_user'));?></li>
              <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_delete_user','2'));?></li>
              <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_delete_user','3'));?></li>
              <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_delete_user','4'));?></li>  
            </ul>   
        </li>
        <li><a href="#">Disable Users</a>
            <ul>
              <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_disable_user'));?></li>
              <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_disable_user','2'));?></li>
              <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_disable_user','3'));?></li>
              <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_disable_user','4'));?></li>  
            </ul>   
        </li>
      </ul>
    </nav>
  </center>
</div>

<!-- Form for Adding user -->
<div style="margin-top:100px !important">
<fieldset class="field_set1">
  <legend>Create User</legend>
   <?php echo $this->Form->create('User'); 
         echo $this->Form->input('username');
         echo $this->Form->input('password');
         echo $this->Form->input('password_confirm',array('type' => 'password', 'label' => 'Confirm Password'));
         echo $this->Form->input('firstname');
         echo $this->Form->input('lastname');
         echo $this->Form->input('email');
         echo $this->Form->input('contact');
         echo $this->Form->input('department_id', array('options' => array('1'=>'1','2'=>'2','3'=>'3','4'=>'4')));
         echo $this->Form->input('role', array('options' => array('admin' => 'admin', 'user' => 'user')));
         echo $this->Form->input('doj');
         echo $this->Form->end(__('Submit')); 
    ?>
</fieldset>
</div>
<!-- Form ends -->
<?php $this->validationErrors; ?> 

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>
