<div id="wrapper">
    <center>
       <nav>
          <ul>
             <li><?php echo $this->Html->link("Home",array('controller' => 'Users','action' => 'index'));?></li>
             <li><a href="#">Details</a></li>
             <li><?php echo $this->Html->link("Salary",array('controller' => 'Calculates','action' => 'user_salary'));?></li>
             <li><?php echo $this->Html->link("Increments",array('controller' => 'Increments','action' => 'user_increments'));?> </li>
             <li><?php echo $this->Html->link("Change Details",array('controller' => 'Users','action' => 'user_change_details'));?> </li>
             <li><?php echo $this->Html->link("Change Password",array('controller' => 'Users','action' => 'change_password'));?></li>
          </ul>
       </nav>
     </center>
</div>

<!-- Show user details -->
<div style="margin-top:100px !important">
<fieldset class="field_set1">
  <legend>Details</legend>
<table>
    <tr>
        <td><?php echo "User id :" ?></td>
        <td><?php echo $user['User']['id']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Username :" ?></td>
        <td><?php echo $user['User']['username']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Firstname :" ?></td>
        <td><?php echo $user['User']['firstname']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Lastname :" ?></td>
        <td><?php echo $user['User']['lastname']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Email id :" ?></td>
        <td><?php echo $user['User']['email']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Contact :" ?></td>
        <td><?php echo $user['User']['contact']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Address :" ?></td>
        <td><?php echo $user['User']['address']; ?></td> 
    </tr> 
    <tr>
        <td><?php echo "Department :" ?></td>
        <td><?php echo $user['Department']['name']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Date of Joining :" ?></td>
        <td><?php echo $user['User']['doj']; ?></td> 
    </tr>
</table>
</fieldset>
</div>
<!-- Show ends -->