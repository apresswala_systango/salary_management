<div id="wrapper">
  <center>
	 <nav>
		<ul>
		  <li><a href="#">Info</a></li>
		  <li><a href="#">View Users</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_user'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_user','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_user','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_user','4'));?></li>	
          </ul>	
		  </li>
		  <li><?php echo $this->Html->link("Add Users",array('controller' => 'Users','action' => 'admin_add_user'));?></li>
		  <li><a href="#">Delete Users</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_delete_user'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_delete_user','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_delete_user','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_delete_user','4'));?></li>  
          </ul>   
      </li>
		  <li><a href="#">Disable Users</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_disable_user'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_disable_user','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_disable_user','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_disable_user','4'));?></li>  
          </ul>   
      </li>
		</ul>
	 </nav>
	</center>
</div>

<!-- Welcome Msg to user -->
<div style="margin-top:100px !important">
<fieldset class="field_set1">
     <legend>Welcome Admin</legend>
     <br>
     <label><?php echo "The Organisation has total {$all} employees";?></label>
     <br><br>
     <label><?php echo "Designing Department has {$designing} designers";?><label>
     <br><br>
     <label><?php echo "Development Department has {$development} developers";?></label>
     <br><br>
     <label><?php echo "Human Resource Department has {$hr} HRs";?></label> 
</fieldset>
</div>
<!-- Msg ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>











