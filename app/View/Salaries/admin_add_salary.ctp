<div id="wrapper">
  <center>
     <nav>
        <ul>
          <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'info1'));?></li>
          <li><a href="#">Add Salary</a></li>
          <li><a href="#">Salary Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_salaries'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_salaries','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_salaries','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_salaries','4'));?></li> 
          </ul> 
          </li>
          <li><a href="#">Increment Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' =>'admin_view_increments'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' =>'admin_view_increments','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' =>'admin_view_increments','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_increments','4'));?></li>    
          </ul>   
      </li>
        </ul>
     </nav>
    </center>
</div>

<!-- Display Users whose salary is not been added -->
<?php
    if($users==null) { ?>
     <center><h1><?php echo "Salary of all the Employees has been added; No Employee left ";?></h1></center>
<?php }
else { ?>

<div style="margin-top:100px !important">
<fieldset class="field_set1">
<legend>Users</legend>
<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>firstname</th>
        <th>lastname</th>
        <th>Action</th>
    </tr>

    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id']; ?></td> 
        <td><?php echo $user['User']['username']; ?></td>
        <td><?php echo $user['User']['firstname']; ?></td>
        <td><?php echo $user['User']['lastname']; ?></td> 
        <td>
            <?php
                echo $this->Html->link(
                    "add Salary",
                    array('controller'=>'Salaries','action' => 'admin_add_salarydetails', $user['User']['id'])
                );
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</fieldset>
</div>
<?php } ?>
<!-- Display ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>