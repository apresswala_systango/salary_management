<div id="wrapper">
  <center>
     <nav>
        <ul>
          <li><?php echo $this->Html->link("Info",array('controller' => 'Users','action' => 'info1'));?></li>
          <li><a href="#">Add Salary</a></li>
          <li><a href="#">Salary Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_salaries'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_salaries','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_salaries','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_salaries','4'));?></li> 
          </ul> 
          </li>
          <li><a href="#">Increment Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' =>'admin_view_increments'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' =>'admin_view_increments','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' =>'admin_view_increments','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_increments','4'));?></li>    
          </ul>   
      </li>
        </ul>
     </nav>
    </center>
</div>

<?php
if(empty($user)) {
  $user=null;
}
?>
<!-- Add the salary of newly created users -->
<div style="margin-top:100px !important">
 <fieldset class="field_set1">
  <legend>Add Salary</legend>    
   <?php 
     echo $this->Form->create('Salary'); 
     echo $this->Form->input('id',array('type' => 'hidden','default'=>$user['Salary']['id']));
     echo $this->Form->input('user_id',array('type' => 'hidden','default'=>$user['User']['id']));
     echo $this->Form->input('basic');
     echo $this->Form->input('houserent');
     echo $this->Form->input('conveyance');
     echo $this->Form->input('medical');
     echo $this->Form->input('lunch'); 
     echo $this->Form->input('added',array('type' => 'hidden','default'=>true));   
     echo $this->Form->end(__('ADD')); 
   ?>
 </fieldset>
</div>
<!-- Add ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>
