<h1>Salaries</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Basic Salary</th>
        <th>House Rent</th>
        <th>Conveyance</th>
        <th>Medical</th>
        <th>Lunch</th>
    </tr>


    <?php foreach ($salaries as $salary): ?>
    <tr>
        <td><?php echo $salary['User']['id']; ?></td> 
        <td><?php echo $salary['User']['username']; ?></td>
        <td><?php echo $salary['Salary']['basic']; ?></td>
        <td><?php echo $salary['Salary']['houserent']; ?></td>
        <td><?php echo $salary['Salary']['conveyance']; ?></td>
        <td><?php echo $salary['Salary']['medical']; ?></td>
        <td><?php echo $salary['Salary']['lunch']; ?></td> 
    </tr>
    <?php endforeach; ?>

</table>