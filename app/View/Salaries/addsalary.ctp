<div>
<?php echo $this->Form->create('Salary'); ?>
    <fieldset>
    <?php 
        echo $this->Form->input('user_id',array('rows' => '1'));
        echo $this->Form->input('basic');
        echo $this->Form->input('houserent');
        echo $this->Form->input('conveyance');
        echo $this->Form->input('medical');
        echo $this->Form->input('lunch');    
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
