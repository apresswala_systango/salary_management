<div id="wrapper">
  <center>
   <nav>
    <ul>
      <li><?php echo $this->Html->link("Info",array('controller' =>'Users','action' => 'info1'));?></li>
      <li><?php echo $this->Html->link("Add Salary",array('controller' =>'Salaries','action' =>'admin_add_salary'));?></li>
      <li><a href="#">Salary Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_salaries'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_salaries','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_salaries','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_salaries','4'));?></li> 
          </ul> 
      </li>
      <li><a href="#">Increment Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' =>'admin_view_increments'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' =>'admin_view_increments','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' =>'admin_view_increments','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_increments','4'));?></li>  
          </ul>   
      </li>
    </ul>
   </nav>
  </center>
</div>

<?php
if(empty($id) || empty($salary)) {
  $id=null;
  $salary=null;
}
?>

<!-- Give salary to the user -->
<div style="margin-top:100px !important">
 <?php
   if (isset($message)) { ?>
      <center><legend><?php echo $message; ?></legend></center>
      <?php }
   else { ?>  
 <fieldset class="field_set1">
  <legend>Add Salary</legend>    
   <?php 
     echo $this->Form->create('Salary'); 
     echo $this->Form->input('Salary.id',array('type' => 'hidden','default'=>$salary['Salary']['id']));
     echo $this->Form->input('Salary.user_id',array('type' => 'hidden','default'=>$salary['Salary']['user_id']));
     echo $this->Form->input('basic_salary',array('default'=>$salary['Salary']['basic'],'class'=>'sum'));
     echo $this->Form->input('Salary.houserent',array('default'=>$salary['Salary']['houserent'],'class'=>'sum'));
     echo $this->Form->input('Salary.conveyance',array('default'=>$salary['Salary']['conveyance'],'class'=>'sum'));
     echo $this->Form->input('Salary.medical',array('default'=>$salary['Salary']['medical'],'class'=>'sum'));
     echo $this->Form->input('Salary.lunch',array('default'=>$salary['Salary']['lunch'],'class'=>'sum')); 
     echo $this->Form->input('Salary.added',array('type' => 'hidden','default'=>$salary['Salary']['added']));  
     echo $this->Form->input('Calculate.0.user_id', array('type' => 'hidden','default'=>$salary['Salary']['user_id']));
     echo $this->Form->input('Calculate.0.salary_id', array('type' => 'hidden','default'=>$salary['Salary']['id']));
     echo $this->Form->input('Calculate.0.extra_pay',array('class'=>'sum'));
     echo $this->Form->input('Calculate.0.extra_pay_desc'); 
     echo $this->Form->input('Calculate.0.deduction',array('class'=>'diff')); 
     echo $this->Form->input('Calculate.0.deduction_desc'); 
     echo $this->Form->input('Calculate.0.gross_sal',array('class'=>'gsal')); 
     echo $this->Form->input('Calculate.0.tax_percent',array('class'=>'tax_pct')); 
     echo $this->Form->input('Calculate.0.tax_amount',array('class'=>'tax_amt'));  
     echo $this->Form->input('Calculate.0.net_sal',array('class'=>'nsal')); 
     echo $this->Form->input('Calculate.0.sal_date');
     echo $this->Form->input('Calculate.0.admin_id', array('type' => 'hidden','default'=>$id)); 
     echo $this->Form->end(__('ADD')); 
   ?>
 </fieldset>
 <?php } ?>
</div>
<!-- Add ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>

<!-- For auto calculation of values -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js "></script>
 <script>

$('.sum').keyup(function () {
    var sum = 0;    
    $('.sum').each(function() {
        sum += Number($(this).val());
    }); 
    $('.gsal').val(sum);
     
});


$('.diff').keyup(function () {
    var sum = 0;
    var diff = 0;    
    $('.sum').each(function() {
        sum += Number($(this).val());
    }); 
    diff = sum - Number($(this).val());  
    $('.gsal').val(diff);
     
});

$('.tax_pct').keyup(function () {
    var gsal=$('.gsal').val();
    var tax_amt= Number($(this).val())/100 * gsal;
     $('.tax_amt').val(tax_amt);
     var nsal= gsal-tax_amt;
     $('.nsal').val(nsal);
});
</script>
<!-- auto calculation ends -->