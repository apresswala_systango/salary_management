<div id="wrapper">
  <center>
	 <nav>
		<ul>
		  <li><?php echo $this->Html->link("Info",array('controller' =>'Users','action' => 'info1'));?></li>
		  <li><?php echo $this->Html->link("Add Salary",array('controller' =>'Salaries','action' =>'admin_add_salary'));?></li>
      <li><a href="#">Salary Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_salaries'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_salaries','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_salaries','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_salaries','4'));?></li>	
          </ul>	
		  </li>
		  <li><a href="#">Increment Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' =>'admin_view_increments'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' =>'admin_view_increments','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' =>'admin_view_increments','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_increments','4'));?></li>  
          </ul>   
      </li>
		</ul>
	 </nav>
	</center>
</div>

<?php
if(empty($id) || empty($salary)) {
  $id=null;
  $salary=null;
}
?>

<!-- Give Increment to the user -->
<div style="margin-top:100px !important">
 <?php
   if (isset($message)) { ?>
      <center><legend><?php echo $message; ?></legend></center>
      <?php }
   else { ?>  
 <fieldset class="field_set1">
  <legend>Add Salary</legend>    
   <?php 
     echo $this->Form->create('Salary'); 
     echo $this->Form->input('Salary.id',array('type' => 'hidden','default'=>$salary['Salary']['id']));
     echo $this->Form->input('Increment.0.user_id', array('type' => 'hidden','default'=>$salary['Salary']['user_id']));
     echo $this->Form->input('Increment.0.salary_id', array('type' => 'hidden','default'=>$salary['Salary']['id']));
     echo $this->Form->input('Increment.0.old_sal',array('default'=>$salary['Salary']['basic'],'label'=>'Old basic salary',
                             'class'=>'amount'));
     echo $this->Form->input('Increment.0.increment',array('class'=>'amount')); 
     echo $this->Form->input('Salary.basic',array('label'=>'New basic salary','class'=>'total_amount'));
     echo $this->Form->input('Increment.0.new_sal', array('type' => 'hidden','class'=>'total_amount1'));
     echo $this->Form->input('Increment.0.incr_date'); 
     echo $this->Form->input('Increment.0.admin_id', array('type' => 'hidden','default'=>$id)); 
     echo $this->Form->end(__('ADD')); 
   ?>
 </fieldset>
 <?php } ?>
</div>
<!-- Increment ends -->

<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>

<!-- For auto calculation of values -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js "></script>
<script>
    $('.amount').keyup(function () {
      var sum = 0;
     
      $('.amount').each(function() {
        sum += Number($(this).val());
      });
     
      $('.total_amount').val(sum);
      $('.total_amount1').val(sum); 
});
</script>
<!-- auto calculation ends -->