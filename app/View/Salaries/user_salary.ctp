<div id="wrapper">
    <center>
       <nav>
          <ul>
             <li><?php echo $this->Html->link("Home",array('controller' => 'Users','action' => 'index'));?></li>
             <li><?php echo $this->Html->link("Details",array('controller' => 'Users','action' => 'user_details'));?></li>
             <li><a href="#">Salary</a></li>
             <li><?php echo $this->Html->link("Increments",array('controller' => 'Increments','action' => 'user_increments'));?> </li>
             <li><?php echo $this->Html->link("Change Password",array('controller' => 'Users','action' => 'change_password'));?></li>
          </ul>
       </nav>
     </center>
</div>

<fieldset class="field_set1">
 <table>
    
    <tr>
        <td><?php echo "Username :" ?></td>
        <td><?php echo $salary['User']['username']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Basic Salary :" ?></td>
        <td><?php echo $salary['Salary']['basic']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "House Rent Allowance :" ?></td>
        <td><?php echo $salary['Salary']['houserent']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Conveyance Allowance :" ?></td>
        <td><?php echo $salary['Salary']['conveyance']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Medical Allowance :" ?></td>
        <td><?php echo $salary['Salary']['medical']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Lunch :" ?></td>
        <td><?php echo $salary['Salary']['lunch']; ?></td> 
    </tr> 
    
  </table>
</fieldset>