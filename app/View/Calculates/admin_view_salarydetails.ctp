<div id="wrapper">
  <center>
	 <nav>
		<ul>
		  <li><?php echo $this->Html->link("Info",array('controller' =>'Users','action' => 'info1'));?></li>
		   <li><?php echo $this->Html->link("Add Salary",array('controller' =>'Salaries','action' =>'admin_add_salary'));?></li>
      <li><a href="#">Salary Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' => 'admin_view_salaries'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' => 'admin_view_salaries','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' => 'admin_view_salaries','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_salaries','4'));?></li>	
          </ul>	
		  </li>
		  <li><a href="#">Increment Options</a>
          <ul>
            <li><?php echo $this->Html->link("All Users",array('controller' =>'Users','action' =>'admin_view_increments'));?></li>
            <li><?php echo $this->Html->link("Designing",array('controller' =>'Users','action' =>'admin_view_increments','2'));?></li>
            <li><?php echo $this->Html->link("Developement",array('controller' =>'Users','action' =>'admin_view_increments','3'));?></li>
            <li><?php echo $this->Html->link("HR",array('controller' => 'Users','action' => 'admin_view_increments','4'));?></li>    
          </ul>   
      </li>
		</ul>
	 </nav>
	</center>
</div>


<!-- Form for getting month and year by user to show salary -->
<div style="margin-top:50px !important">
<fieldset class="salary_form">
    <table>
       <tr>
          <td><?php echo $this->Form->year('year', 2000, date('Y'),array('id'=>'year'));?></td>
          <td><?php echo $this->Form->month('month',array('id'=>'month')); ?></td>
          <td><?php echo $this->Form->input('user_id',array('type'=>'hidden','id'=>'user_id','default'=>$id)); ?>
       </tr>
   </table>
</fieldset>
</div>
<!-- Form end -->

<div id="result"></div>
<center><h1 id="message">Please select a year and a month</h1></center>
<!-- Showing requested User Salary to admin-->
<script>
    $(document).ready(function() {
        $('#year').change(function(){
            $.ajax({

                type: "GET",
                url: "http://localhost/salary_management/Calculates/admin_view_salarydetails_ajax",
                data: { year:$("#year").val() , month:$("#month").val() , user_id:$('#user_id').val() },
                success: function(msg){
                    document.getElementById("message").innerHTML = "";
                    $('#result').html(msg);
                }

            }); // Ajax Call
        }); //event handler

        $('#month').change(function(){
            $.ajax({

                type: "GET",
                url: "http://localhost/salary_management/Calculates/admin_view_salarydetails_ajax",
                data: { year:$("#year").val() , month:$("#month").val() , user_id:$('#user_id').val() },
                success: function(msg){
                    document.getElementById("message").innerHTML = "";
                    $('#result').html(msg);
                }

            }); // Ajax Call
        }); //event handler

    }); //document.ready
</script>


<?php
   echo $this->Html->link("Back to Home", array('controller' => 'Users','action'=> 'admin_index'), array( 'class' => 'button1'))
?>