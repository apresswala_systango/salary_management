<?php 
  if(empty($salary)) { ?>
    <center><legend>No Records Found</legend></center>
<?php  
  }
  else { ?>
<fieldset class="field_set1">
 <legend>Your Salary</legend> 
 <table>
    <tr>
        <td><?php echo "User ID :" ?></td>
        <td><?php echo $salary['Salary']['user_id']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Basic Salary :" ?></td>
        <td><?php echo $salary['Salary']['basic']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "House Rent Allowance :" ?></td>
        <td><?php echo $salary['Salary']['houserent']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Conveyance Allowance :" ?></td>
        <td><?php echo $salary['Salary']['conveyance']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Medical Allowance :" ?></td>
        <td><?php echo $salary['Salary']['medical']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Lunch :" ?></td>
        <td><?php echo $salary['Salary']['lunch']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Extra Payment :" ?></td>
        <td><?php echo $salary['Calculate']['extra_pay']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Description :" ?></td>
        <td><?php echo $salary['Calculate']['extra_pay_desc']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Deduction :" ?></td>
        <td><?php echo $salary['Calculate']['deduction']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Description :" ?></td>
        <td><?php echo $salary['Calculate']['deduction_desc']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Gross Salary :" ?></td>
        <td><?php echo $salary['Calculate']['gross_sal']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Tax :" ?></td>
        <td><?php echo $salary['Calculate']['tax_amount']; ?></td> 
    </tr>  
    <tr>
        <td><?php echo "Net Salary :" ?></td>
        <td><?php echo $salary['Calculate']['net_sal']; ?></td> 
    </tr>
    <tr>
        <td><?php echo "Date :" ?></td>
        <td><?php echo $salary['Calculate']['sal_date']; ?></td> 
    </tr>
 </table>
</fieldset>
<?php } ?> 
<!-- Show end -->
