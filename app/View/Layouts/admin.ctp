<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Salary Management');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
        echo $this->Html->script('jquery-2.1.1');     
		echo $this->Html->css('ddemployee');
        
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

<div>

			<?php echo $this->Session->flash(); ?>
            
             <!--Logout link-->
             <?php if($this->Session->read('Auth.User')) {  ?>  
             <p align="right"> <?php echo $this->Html->link('Logout', array('controller'=>'users', 'action'=>'logout')); ?> </p> 
             <?php } else {} ?> 
            <!--Logout link ends here--> 

			<?php echo $this->fetch('content'); ?>
</div>

<div id="footer">
			
</div>
<!--	<?php echo $this->element('sql_dump'); ?>  -->
<?php echo $this->Js->writeBuffer(); ?>
</body>
</html>
