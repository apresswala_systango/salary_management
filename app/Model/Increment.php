<?php
  class Increment extends AppModel {
    
      public $validate = array(
         'old_sal' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A value is required'
             ),
            'ensure' => array(
                'rule' => array('comparison', '>=', 0),
                'message' => 'Should not be negative'
             )
          ),
          'increment' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A value is required'
             ),
            'ensure' => array(
                'rule' => array('comparison', '>=', 0),
                'message' => 'Should not be negative'
             )
          )

      );

  }
?>