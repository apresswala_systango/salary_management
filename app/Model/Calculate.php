<?php
class Calculate extends AppModel {

    public $belongsTo = array(
        'Salary' => array(
            'className' => 'Salary',
            'foreignKey' => 'salary_id'
        )
    );

    public $validate = array( 
           'extra_pay' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Should not be negative'
               )
            ),
            'deduction' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Should not be negative'
               )
            ),
            'gross_sal' => array(
              'required' => array(
                  'rule' => array('notEmpty'),
                  'message' => 'A value is required'
               ),
               'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'message' => 'Should not be negative'
               )
            ),
            'tax_percent' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Percentage should not be negative'
               )
            ),
            'tax_amount' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Tax should not be negative'
               )
            ),
            'net_sal' => array(
              'required' => array(
                  'rule' => array('notEmpty'),
                  'message' => 'A value is required'
               ),
               'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'message' => 'Should not be negative'
               )
            ), 
    );

} 
?>