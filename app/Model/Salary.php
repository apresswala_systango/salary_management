<?php
class Salary extends AppModel {

	public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    public $hasMany = array(
        'Calculate' => array(
            'className' => 'Calculate',
            'foreignKey' => 'salary_id',
        ),
        'Increment' => array(
            'className' => 'Increment',
            'foreignKey' => 'salary_id',
        )
    );

    public $validate = array(
           'basic' => array(
              'required' => array(
                  'rule' => array('notEmpty'),
                  'message' => 'A value is required'
               ),
               'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'message' => 'Salary should not be negative'
               )
            ),
           'houserent' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Should not be negative'
               )
            ),
            'conveyance' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Should not be negative'
               )
            ),
            'medical' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Should not be negative'
               )
            ),
            'lunch' => array(
              'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'allowEmpty' => true,
                   'message' => 'Should not be negative'
               )
            ),
            'basic_salary' => array(
              'required' => array(
                  'rule' => array('notEmpty'),
                  'message' => 'A value is required'
               ),
               'ensure' => array(
                   'rule' => array('comparison', '>=', 0),
                   'message' => 'Salary should not be negative'
               )
            ),
    );  

}

?>