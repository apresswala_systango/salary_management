<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    
    public $hasOne = array(
        'Salary' => array(
            'className' => 'Salary',
            'foreignKey' => 'user_id',
            'dependent' => true
        )
    );

    public $belongsTo = array(
        'Department' => array(
            'className' => 'Department',
            'foreignKey' => 'department_id'
        )
    );

    public $hasMany = array(
        'Increment' => array(
            'className' => 'Increment',
            'foreignKey' => 'user_id',
            'dependent' => true
        ),
        'Calculate' => array(
            'className' => 'Calculate',
            'foreignKey' => 'user_id',
            'dependent' => true
        )
    );

    
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
             ),
             'min' => array( 
               'rule' => array('minLength', 6), 
               'message' => 'Usernames must be at least 6 characters.'
             )
        ),
        'password' => array( 
            'min' => array( 
               'rule' => array('minLength', 6), 
               'message' => 'Password must be at least 6 characters.' 
            ), 
            'required' => array( 
               'rule' => 'notEmpty', 
               'message'=>'Please enter a password.' 
            )
        ), 
        'password_confirm' => array(  
            'required' => array( 
               'rule' => 'notEmpty', 
               'message'=>'Please confirm the password.' 
            ),
            'match' => array( 
               'rule' => 'validatePasswordConfirm', 
               'message' => 'Passwords do not match' 
            )
        ), 
        'role' => array(
            'valid' => array(
               'rule' => array('inList', array('admin', 'user')),
               'message' => 'Please enter a valid role',
               'allowEmpty' => false
            )
        ),
        'contact' => array(
            'required' => array(
               'rule' => array('notEmpty'),
               'message' => 'A contact is required'
             ),
            'min' => array( 
               'rule' => array('minLength', 10), 
               'message' => 'Contact must be at least 10 numbers.'
             ),
            'number' => array( 
               'rule' => 'numeric', 
               'message' => 'Contact must be numeric.'
             ),
        ),
        'firstname' => array(
            'required' => array(
               'rule' => array('notEmpty'),
               'message' => 'Firstname is required'
             )
        ),
        'lastname' => array(
            'required' => array(
               'rule' => array('notEmpty'),
               'message' => 'Lasttname is required'
             )
        ),
        'email' => array(
            'required' => array(
               'rule' => array('notEmpty'),
               'message' => 'Email id is required'
             ),
            'valid email' => array( 
               'rule' => 'email', 
               'message' => 'Please give a valid email id.'
             )
        ),
    );

    public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new BlowfishPasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
        );
    }
    return true;
    }

    public function validatePasswordConfirm($data) { 
        if ($this->data['User']['password'] !== $data['password_confirm']) { 
            return false; 
        } 
        return true; 
    }  

}

?>