<?php
class IncrementsController extends AppController {

    var $layout = 'default';
    
    // User see their Increments
    public function user_increments() {
      $this->layout='employee';
      $id=$this->Auth->User('id');
      $increments=$this->Increment->findAllByUser_id($id,array(),array('Increment.incr_date' => 'desc'));
      $this->set('increments',$increments);
    }
    
    // Admin see User's Increments
    public function admin_view_incrementdetails($id=null) {
      $this->layout='admin';
      $increments=$this->Increment->findAllByUser_id($id,array(),array('Increment.incr_date' => 'desc'));
      $this->set('increments',$increments);
    }

    public function isAuthorized($user) {
       if (isset($user['role']) && $user['role']==='user') {
          if (in_array($this->action, array('user_increments'))) { 
             return true;
          }
       }
       return parent::isAuthorized($user);
    }

}
?>