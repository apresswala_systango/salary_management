<?php
class SalariesController extends AppController {

    var $layout = 'default';

    // Set users whose salary is not been added
    public function admin_add_salary() {
       $this->layout='admin';  
       $users=$this->Salary->find('all' , array('conditions'=>array('Salary.added'=>false)));
       $this->set('users',$users);
    }
    
    // Add salary of user selected from above action
   	public function admin_add_salarydetails($id=null) {
       $this->layout='admin';
       if(empty($this->request->data)) {
         $user=$this->Salary->findByUser_id($id);
         $this->set('user',$user);    
       }
       if (!empty($this->request->data)) {
          $this->Salary->set($this->request->data);      
          if ($this->Salary->validates()) { // check validations 
              $this->request->data['Salary']['admin_id']=$this->Auth->user('id');
              if ($this->Salary->save($this->request->data)) {
                 $this->Session->setFlash(__('The salary has been saved'));
                 return $this->redirect(array('controller' => 'salaries','action' => 'admin_add_salary'));
              }
              $this->Session->setFlash('The salary could not be saved. Please, try again.');
          }
          pr($this->Salary->validationErrors);
       } 
    }
   
    // Give salary to user
    public function admin_give_salary($id=null) {
       $this->layout='admin';
       if(empty($this->request->data)) {
         $salary=$this->Salary->findByUser_id($id);
         if ($salary['Salary']['basic']==null) {
             $message="No Salary record found , add a salary first";
             $this->set('message',$message);
         }
         else {
         $this->set('salary',$salary); 
         $id=$this->Auth->user('id');
         $this->set('id',$id); 
         }   
       }
       if (!empty($this->request->data)) {
          $this->Salary->Calculate->set($this->request->data);      
          if ($this->Salary->Calculate->validates()) {  // check validations
             unset($this->Salary->Calculate->validate['salary_id']);
             if ($this->Salary->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('The salary has been saved'));
                return $this->redirect(array('controller' => 'users','action' => 'admin_view_salaries'));
             }
             $this->Session->setFlash('The salary could not be saved. Please, try again.');            
          }
          
       }
    }
   
   // Give increment to user
   public function admin_give_increment($id=null) {
       $this->layout='admin';
       if(empty($this->request->data)) {
         $salary=$this->Salary->findByUser_id($id);
         if ($salary['Salary']['basic']==null) {
             $message="No Salary record found , add a salary first";
             $this->set('message',$message);
         }
         else {
         $this->set('salary',$salary); 
         $id=$this->Auth->user('id');
         $this->set('id',$id); 
         }   
       }
       if (!empty($this->request->data)) {
          $this->Salary->Increment->set($this->request->data);      
          if ($this->Salary->Increment->validates()) {  // check validations
             unset($this->Salary->Increment->validate['salary_id']);
             if ($this->Salary->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('The increment has been saved'));
                return $this->redirect(array('controller' => 'users','action' => 'admin_view_increments'));
             }
             $this->Session->setFlash('The increment could not be saved. Please, try again.');
          }
       }
   }

}
?>