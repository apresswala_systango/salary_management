<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {
   
    var $layout = 'default';
    
    public function beforeFilter() {  
       parent::beforeFilter();
       $this->Auth->allow('logout');
    }
    
    // Admin Module Starts
    public function admin_index() {       
       $this->layout = 'admin'; 
    }
    
    // 1. Admin User Management Module starts
    // Home
    public function info() {                         
       $this->layout = 'admin';
       $all=$this->User->find('count',array('conditions'=>array('enable'=>true)));
       $designing=$this->User->find('count',array('conditions' => array('department_id' => 2,'enable'=>true)));
       $development=$this->User->find('count',array('conditions' => array('department_id' => 3,'enable'=>true)));
       $hr=$this->User->find('count',array('conditions' => array('department_id' => 4,'enable'=>true)));
       $this->set(compact('all','designing', 'development', 'hr'));
    }
    
    // View : all the user at once as well as department wise
    public function admin_view_user() {                                
       $this->layout = 'admin'; 
       $id=$this->request->params['pass'];
       $condition1=array('enable'=>true);
       if($id==null) { 
         $this->layout = 'admin'; 
         $users = $this->User->find('all',array('conditions' => $condition1));
         $this->set('users', $users);
       }
       else {
         $condition2=array('department_id' => $id);
         $users = $this->User->find('all',array('conditions' => array($condition1,$condition2)));
         $this->set('users', $users);     
       }
    }
    
    // Details of User selected from above action
    public function admin_view_userdetails($id=null) {
       $this->layout = 'admin';   
       $user = $this->User->findById($id);
       $this->set('user', $user);
    }

    // Add a user and make entry in Salary table 
    public function admin_add_user() {
       $this->layout = 'admin'; 
       $this->User->set($this->request->data);      
       if ($this->User->validates()) { // check validations       
       if ($this->request->is('post')) {
          $this->User->create();
          $this->request->data['User']['admin_id'] = $this->Auth->user('id');
          if (!empty($this->request->data)) {
             $user = $this->User->save($this->request->data);
             $this->request->data['Salary']['user_id'] = $this->User->id; 
             $this->User->Salary->save($this->request->data); 
             return $this->redirect(array('controller' => 'Users','action' => 'admin_view_user'));
          }
          $this->Session->setFlash(('The user could not be saved. Please, try again.'));  
          
         
        } 
      }
    }
    
    // Edit user: seting data for the edit form
    public function admin_edit_user($id=null) {
       $this->layout = 'admin';
       if (empty($this->request->data)) {
          $user = $this->User->findById($id);
          $this->set('user', $user);
       }
       if (!empty($this->request->data)) {
          if ($this->request->is(array('post', 'put'))) {
             $this->User->set($this->request->data);      
             if ($this->User->validates()) { // check validations
                $this->User->id = $id;
                if ($this->User->save($this->request->data)) {
                   $this->Session->setFlash(__('User has been updated.'));
                   return $this->redirect(array('action' => 'admin_view_user'));
                }
                $this->Session->setFlash(('Unable to Edit the User.'));
              }
          }
       } 
    }
      
    // delete user: setting data for deletion form
    public function admin_delete_user() {
       $this->layout = 'admin'; 
       $id=$this->request->params['pass'];
       $condition1=array('enable'=>true);
       if($id==null) { 
         $this->layout = 'admin'; 
         $users = $this->User->find('all',array('conditions' => $condition1));
         $this->set('users', $users);
       }
       else {
         $condition2=array('department_id' => $id);
         $users = $this->User->find('all',array('conditions' => array($condition1,$condition2)));
         $this->set('users', $users);     
       }
    }
    
    // delete the users selected from above action
    public function delete_selected() {
       foreach($this->data['Users'] as $key => $value) {
          if($value != 0) {
                $this->User->delete($value,true);
          }
       }
       return $this->redirect(array('action' => 'admin_view_user'));
    }
    
    // disable user: setting data for disabling form
    public function admin_disable_user() {
       $this->layout = 'admin'; 
       $id=$this->request->params['pass'];
       $condition1=array('enable'=>true);
       if($id==null) { 
         $this->layout = 'admin'; 
         $users = $this->User->find('all',array('conditions' => $condition1));
         $this->set('users', $users);
       }
       else {
         $condition2=array('department_id' => $id); 
         $users = $this->User->find('all',array('conditions' => array($condition1,$condition2)));
         $this->set('users', $users);     
       }
    }
    
    // disable the users selected from above action
    public function disable_selected() {
       foreach($this->data['Users'] as $key => $value) {
          if($value != 0) {
            $this->User->id = $value;
            $this->User->saveField('enable', false);
          }
       }
       $this->Session->setFlash(('Disabling Successfull'));
       return $this->redirect(array('action' => 'info'));
    }
    // 1. Admin User Management Module ends

    
    // 2. Admin Salary Management Module
    // Home
    public function info1() {
       $this->layout = 'admin';
       
    }

    public function email(){
       
        $Email = new CakeEmail();
        $Email->config('smtp')
              ->emailFormat('html')  
              ->from('rajuraj462@gmail.com')        
              ->to('ali.pressy@gmail.com')
              ->subject("abc") 
              ->send('message');
    }
    
    // To see or give salary : show all the user at once as well as department wise
    public function admin_view_salaries() {                                
       $this->layout = 'admin'; 
       $id=$this->request->params['pass'];
       $condition1=array('enable'=>true);
       if($id==null) { 
         $this->layout = 'admin'; 
         $users = $this->User->find('all',array('conditions' => $condition1));
         $this->set('users', $users);
       }
       else {
         $condition2=array('department_id' => $id);
         $users = $this->User->find('all',array('conditions' => array($condition1,$condition2)));
         $this->set('users', $users);      
       }
    }

     // To see or give increment : show all the user at once as well as department wise
    public function admin_view_increments() {                                
       $this->layout = 'admin'; 
       $id=$this->request->params['pass'];
       $condition1=array('enable'=>true);
       if($id==null) { 
         $this->layout = 'admin'; 
         $users = $this->User->find('all',array('conditions' => $condition1));
         $this->set('users', $users);
       }
       else {
         $condition2=array('department_id' => $id);
         $users = $this->User->find('all',array('conditions' => array($condition1,$condition2)));
         $this->set('users', $users);    
       }
    }
    // 2. Admin Salary Management Module ends
    
    // User Module Starts
    // User homepage
    public function index() {
       $this->layout = 'employee'; 
       $id=$this->Auth->user('id');
       $user = $this->User->findById($id);
       $this->set('user', $user);
       $date1=$user['User']['doj'];
       $date2=date("Y-m-d");
       $datetime1 = new DateTime($date1);
       $datetime2 = new DateTime($date2);
       $interval = $datetime2->diff($datetime1);
       $period=$interval->format('%y years %m months and %d days');
       $this->set('period', $period);
    }
    
    // details of logged in user
    public function user_details() {
       $this->layout = 'employee'; 
       $id=$this->Auth->user('id');
       $user = $this->User->findById($id);
       $this->set('user', $user);
    }
    
    // Change password
    public function change_password() {
       $this->layout = 'employee'; 
       if (empty($this->request->data)) {
          $id=$this->Auth->user('id');
          $user = $this->User->findById($id);
          $this->set('user', $user);
       }
       if (!empty($this->request->data)) {
          $this->User->set($this->request->data);      
          if ($this->User->validates()) { // check validations
             if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Password has been changed.'));
                return $this->redirect(array('action' => 'index'));
             }
             $this->Session->setFlash(__('Unable to changed the Password.'));
             return $this->redirect(array('action' => 'change_password'));  
          }
          pr($this->User->validationErrors);
       }
    }
     
    // user update their details
    public function user_change_details(){
       $this->layout='employee';  
       if (empty($this->request->data)) {
          $id=$this->Auth->user('id');
          $user = $this->User->findById($id);
          $this->set('user',$user);
       }
       if (!empty($this->request->data)) {
          if ($this->request->is(array('post', 'put'))) {
             $this->User->set($this->request->data);      
             if ($this->User->validates()) { // check validations    
                if ($this->User->save($this->request->data)) {
                   $this->Session->setFlash(__('Your details has been updated.'));
                   return $this->redirect(array('action' => 'user_details'));
                }
                $this->Session->setFlash(__('Unable to Edit the User.'));
             }
          }
        }

    }
    // User Module ends
     
    // login authentication  
    public function login() {
       $this->layout='employee';  
       if ($this->request->is('post')) {
          if ($this->Auth->login()) {
             return $this->redirect($this->Auth->redirect());
          }
          $this->Session->setFlash(__('Invalid username or password, try again'));
       }
    }
    
    // logout
    public function logout() {
       return $this->redirect($this->Auth->logout());
    } 
    
    // check if user is enabled
    public function check_enable() {
      $id=$this->Auth->user('id');
      $user=$this->User->findById($id);
      $enable=$user['User']['enable'];
      if(!$enable) {
        $this->Session->setFlash(__('You are not a valid User'));
        return $this->redirect(array('action' => 'logout'));
      }
      else {
        return $this->redirect(array('action' => 'index')); 
      }

      
    }


    public function isAuthorized($user) {
       if (isset($user['role']) && $user['role']==='user') {
          if (in_array($this->action, array('index','user_details','check_enable','change_password','user_change_details','change_pwd'))) { 
             return true;
          }
       }
       return parent::isAuthorized($user);
    }

}

?>