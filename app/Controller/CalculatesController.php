<?php
App::uses('AppController', 'Controller');

class CalculatesController extends AppController {

    var $layout = 'default';
   
    // User see their salary
    public function user_salary() {
       $this->layout='employee';
       $id=$this->Auth->user('id');
       $condition1=array('Calculate.user_id' => $id);
       if (empty($this->request->data)) {
          $salary = $this->Calculate->find('first' , array('conditions' => $condition1,
                    'order' => array('Calculate.sal_date DESC')));
    	    $this->set('salary',$salary); 
       }
       else {
          $year=$this->request->data['Calculate']['year'];
          $month=$this->request->data['Calculate']['month'];
          $mm = array_shift($month);
          $yyyy = array_shift($year);
          $condition2 = array("Calculate.sal_date LIKE" => "$yyyy-$mm-%");
          $salary=$this->Calculate->find('first', array('conditions' => array($condition2,$condition1))); 
          $this->set('salary',$salary);
       }
    }
    
    // Admin see user's salary
    public function admin_view_salarydetails($id=null) {
       $this->layout='employee';
       $this->set('id',$id);
    }

    public function isAuthorized($user) {
       if (isset($user['role']) && $user['role']==='user') {
          if (in_array($this->action, array('user_salary'))) { 
             return true;
          }
       }
       return parent::isAuthorized($user);
    }

    public function admin_view_salarydetails_ajax() {
       $this->layout = 'salary'; 
       $yyyy=$_GET['year'];
       $mm=$_GET['month'];
       $id=$_GET['user_id'];
       $condition1=array('Calculate.user_id' => $id);
       $condition2 = array("Calculate.sal_date LIKE" => "$yyyy-$mm-%");
       $salary=$this->Calculate->find('first', array('conditions' => array($condition2,$condition1))); 
       $this->set('salary',$salary);
    }

}
?>